<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use  Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table='user';

    public $primaryKey="id_akun";

    protected $fillable = [
         'id_akun','nm_akun','username', 'password','isadmin','alamat','telepon','negara',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function income()
     {
         return $this->hasMany('App\Income');
     }

   public function outcome()
    {
        return $this->hasMany('App\Outcome');
    }

  public function kategori()
   {
       return $this->hasMany('App\Kategori');
   }

}
