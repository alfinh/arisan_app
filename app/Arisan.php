<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arisan extends Model
{
    protected $table = 'data_arisan';

    protected $fillable = ['nm_anggota','alamat','keterangan','status_bayar','status_menang'];
    
}
