<?php

namespace App\Exports;
use App\User;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;

class UserExport implements FromQuery, WithHeadings, ShouldAutoSize
{

  use Exportable;

    // public function __construct(int $year)
    //   {
    //       $this->year = $year;
    //   }

      public function forDate(string $date, $datee)
     {
         $this->date = $date;
         $this->datee = $datee;

         return $this;
     }

    public function query()
    {
      return User::query()->whereBetween('created_at',[$this->date , $this->datee]);
    }
    public function headings(): array
    {
      return [
           'Id Akun',
           'Nama Akun',
           'Username',
           'Hak Akses',
           'alamat',
           'telepon',
           'negara',
           'created_at',
           'updated_at'
       ];
    }
}
