<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class IsUser
{
  public function handle($request, Closure $next)
  {
     if (Auth::user() &&  Auth::user()->isadmin == 0) {
            return $next($request);
     }

    return redirect('/');
  }
}
