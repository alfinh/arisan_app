<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Kategori;
use App\Income;
use App\Outcome;
use App\OauthAcessToken;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
  public $successStatus = 200;
/**
   * login api
   *
   * @return \Illuminate\Http\Response
   */
  public function login(){
      if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
          $user = Auth::user();
          $success['message'] = 'Login Berhasil';
          $success['status'] = true;
          $success['data'] = array('token' =>  $user->createToken('MyApp')-> accessToken);

          return response()->json($success);
      }
      else{
        $fails['message'] = 'Login Gagal';
        $fails['status'] = false;
        $fails['data'] = null;

        return response()->json($fails);
      }
  }
/**
   * Register api
   *
   * @return \Illuminate\Http\Response
   */
  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
          'nm_akun' => 'required',
          'username' => 'required',
          'password' => 'required',
          'alamat' => 'required',
          'telepon' => 'required',
          'negara' => 'required'
            ]);
    if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);
            }

    $input = $request->all();
      $input['password'] = bcrypt($input['password']);
      $user = User::create($input);

      $success['message'] = 'Register Berhasil';
      $success['status'] = true;
      $success['data'] =
      array('token'    =>  $user->createToken('MyApp')-> accessToken,
            'nm_user'  =>  $user->nm_user,
            'email'    =>  $user->email,
            'alamat'   =>  $user->alamat,
            'telepon'  =>  $user->telepon,
            'id_toko'  =>  $user->id_toko );

      return response()->json($success);
  }
/**
   * details api
   *
   * @return \Illuminate\Http\Response
   */
  public function details()
  {
    $user = Auth::user();
    $success['message'] = 'Login saat ini';
    $success['status'] = true;
    $success['data'] =
    array('user'    => $user );

    return response()->json($success);
  }

  public function logoutApi()
  {
    Auth::user()->token()->delete();
    $success['message'] = 'Logout Berhasil';
    $success['status'] = true;
    $success['data'] = null;
    return response()->json($success);

  }

  public function update(Request $request)
     {
         $id = Auth::user()->id_akun;
         $user= User::findOrFail($id);
         $user->update($request->all());

         $success['message'] = 'Update Profile';
         $success['status'] = true;
         $success['data'] = $user;

         return response()->json($success);

         // echo "check";die;
     }

     public function delete()
      {
          $id=Auth::user()->id_akun;
          $outcome = DB::table('tbl_outcome')->where('id_akun','=',$id);
          $outcome->delete();
          $income = DB::table('tbl_income')->where('id_akun','=',$id);
          $income->delete();
          $kategori = DB::table('tb_m_kategori')->where('id_akun','=',$id);
          $kategori->delete();
          $user = User::findOrFail($id);
          $user->delete();

          $success['message'] = 'Delete Akun Berhasil';
          $success['status'] = true;
          $success['data'] = null;

          return response()->json($success);
      }

}
