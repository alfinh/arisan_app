<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Income;
use Auth;

class IncomeController extends Controller
{
  public function index(){

  $id = Auth::user()->id_akun;
  $check = DB::table('oauth_access_tokens')->where('user_id','=',$id)->count();
      if ( $check > 0) {
          $auth = Auth::user()->id_akun;
          $income = DB::select("SELECT * FROM tbl_income where id_akun = '$auth'");
          $success['message'] = 'Data Income';
          $success['status'] = true;
          $success['data'] = $income;

          return response()->json($success);
      }else{
        $fails["message"] = 'Anda Belum melakukan Login';
        $fails['status'] = false;
        $fails['data'] = null;
        return response()->json($fails);
      }
 }

public function show($id){
      $income = DB::select("SELECT * FROM tbl_income where id_income ='$id'");
      $success['message'] = 'Show Income';
      $success['status'] = true;
      $success['data'] = $income;

      return response()->json($success);

}

public function store(Request $request){

  $income =  Income::create($request->all());
  $success['message'] = 'Add Income';
  $success['status'] = true;
  $success['data'] = $income;

  return response()->json($success);


}

public function update(Request $request, $id)
 {
     $income = Income::findOrFail($id);
     $income->update($request->all());

     $success['message'] = 'Update Income';
     $success['status'] = true;
     $success['data'] = $income;

     return response()->json($success);
 }

 public function delete(Request $request, $id)
  {
      $income = Income::findOrFail($id);
      $income->delete();

      $success['message'] = 'Delete Income Berhasil';
      $success['status'] = true;
      $success['data'] = null;

      return response()->json($success);
  }

}
