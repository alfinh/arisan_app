<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kategori;
use Auth;

class KategoriController extends Controller
{
  public function index(){

  $id = Auth::user()->id_akun;
  $check = DB::table('oauth_access_tokens')->where('user_id','=',$id)->count();
      if ( $check > 0) {
          $auth = Auth::user()->id_akun;
          $kategori = DB::select("SELECT * FROM tb_m_kategori where id_akun = '$auth'");
          $success['message'] = 'Data kategori';
          $success['status'] = true;
          $success['data'] = $kategori;

          return response()->json($success);
      }else{
        $fails["message"] = 'Anda Belum melakukan Login';
        $fails['status'] = false;
        $fails['data'] = null;
        return response()->json($fails);
      }
 }

public function show($id){
      $kategori = DB::select("SELECT * FROM tb_m_kategori where kategori ='$id'");
      $success['message'] = 'Show kategori';
      $success['status'] = true;
      $success['data'] = $kategori;

      return response()->json($success);

}

public function store(Request $request){

  $kategori =  Kategori::create($request->all());
  $success['message'] = 'Add kategori';
  $success['status'] = true;
  $success['data'] = $kategori;

  return response()->json($success);


}

public function update(Request $request, $id)
 {
     $kategori = Kategori::findOrFail($id);
     $kategori->update($request->all());

     $success['message'] = 'Update kategori';
     $success['status'] = true;
     $success['data'] = $kategori;

     return response()->json($success);
 }

 public function delete(Request $request, $id)
  {
      $kategori = Kategori::findOrFail($id);
      $kategori->delete();

      $success['message'] = 'Delete kategori Berhasil';
      $success['status'] = true;
      $success['data'] = null;

      return response()->json($success);
  }

}
