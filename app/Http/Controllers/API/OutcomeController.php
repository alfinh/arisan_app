<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Outcome;
use Auth;

class OutcomeController extends Controller
{
      public function index(){

      $id = Auth::user()->id_akun;
      $check = DB::table('oauth_access_tokens')->where('user_id','=',$id)->count();
          if ( $check > 0) {
              $auth = Auth::user()->id_akun;
              $outcome = DB::select("SELECT * FROM tbl_outcome where id_akun = '$auth'");
              $success['message'] = 'Data Outcome';
              $success['status'] = true;
              $success['data'] = $outcome;

              return response()->json($success);
          }else{
            $fails["message"] = 'Anda Belum melakukan Login';
            $fails['status'] = false;
            $fails['data'] = null;
            return response()->json($fails);
          }
     }

    public function show($id){
          $outcome = DB::select("SELECT * FROM tbl_outcome where id_outcome ='$id'");
          $success['message'] = 'Show outcome';
          $success['status'] = true;
          $success['data'] = $outcome;

          return response()->json($success);

    }

    public function store(Request $request){

      $outcome =  Outcome::create($request->all());
      $success['message'] = 'Add Outcome';
      $success['status'] = true;
      $success['data'] = $outcome;

      return response()->json($success);


    }

    public function update(Request $request, $id)
     {
         $outcome = Outcome::findOrFail($id);
         $outcome->update($request->all());

         $success['message'] = 'Update Outcome';
         $success['status'] = true;
         $success['data'] = $outcome;

         return response()->json($success);
     }

     public function delete(Request $request, $id)
      {
          $outcome = Outcome::findOrFail($id);
          $outcome->delete();

          $success['message'] = 'Delete Outcome Berhasil';
          $success['status'] = true;
          $success['data'] = null;

          return response()->json($success);
      }

}
