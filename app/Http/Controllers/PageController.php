<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;
use DataTables;
use Auth;
use App\Arisan;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){

      return view('page.dashboard');

    }

    public function getarisan(){

      return view('page.dataarisan');

    }

    public function json(Request $request){

      $data = Arisan::all();
        return Datatables::of($data)
        ->addColumn('action', function($data){
        if($data->status_bayar == 'Sudah Bayar'){
        return '<a href="edit_arisan/'.$data->id_data.'" class="btn btn-xs btn-primary btn-fill " id="">Ubah</a>
                <a href="delete_arisan/'.$data->id_data.'" class="btn btn-xs btn-danger btn-fill delete" id="delete">Delete</a>';
       }else{
         return '<a href="bayar_arisan/'.$data->id_data.'" class="btn btn-xs btn-success btn-fill " id="">Bayar</a>
                 <a href="edit_arisan/'.$data->id_data.'" class="btn btn-xs btn-primary btn-fill " id="">Ubah</a>
                 <a href="delete_arisan/'.$data->id_data.'" class="btn btn-xs btn-danger btn-fill delete" id="delete">Delete</a>';
       }
      })

      ->make(true);

    }

    public function addanggota(Request $request){

      $validate = \Validator::make($request->all(), [
              'nm_anggota' => 'required',
              'alamat'=> 'required',
              'keterangan' => 'required'

              ],

              $after_save = [
                  'alert' => 'danger',
                  'icon' => 'attention',
                  'title' => 'kesalahan !',
                  'text-1' => 'Ini terjadi dalam menginput',
                  'text-2' => 'Silakan coba lagi !'
              ]);

          if($validate->fails()){
              return redirect()->back()->with('after_save', $after_save);
          }

          $after_save = [
                    'alert' => 'success',
                    'icon' => 'check',
                    'title' => 'Berhasil ! ',
                    'text-1' => 'Data Anggota ',
                    'text-2' => 'Telah tertambah.'
                ];


      $data = new Arisan();
         $data->nm_anggota = $request->nm_anggota;
         $data->alamat = $request->alamat;
         $data->keterangan = $request->keterangan;
         $data->status_bayar = 'Belum Bayar';
         $data->status_menang = 'Belum Menang';
         $data->save();

      return redirect()->back()->with('after_save', $after_save);
    }

    public function showarisan($id){

      $data = \App\Arisan::where('id_data', $id)->first();
      return view('page.editdata',compact('data'));

    }

    public function updatearisan(Request $request){

      $validate = \Validator::make($request->all(), [
              'nm_anggota' => 'required',
              'alamat' => 'required',
              'keterangan' => 'required'

              ],

              $after_save = [
                  'alert' => 'danger',
                  'icon' => 'attention',
                  'title' => 'kesalahan !',
                  'text-1' => 'Ini terjadi dalam menginput',
                  'text-2' => 'Silakan coba lagi !'
              ]);

          if($validate->fails()){
              return redirect('/dataarisan')->with('after_save', $after_save);
          }

          $after_save = [
                    'alert' => 'success',
                    'icon' => 'check',
                    'title' => 'Berhasil ! ',
                    'text-1' => 'Data Anggota ',
                    'text-2' => 'Telah diubah.'
                ];
          $id= $request->input('id_data');
          $data= Arisan::where('id_data', $id);
          $data->update([
            'nm_anggota' => $request->nm_anggota,
            'alamat' => $request->alamat,
            'keterangan' => $request->keterangan
          ]);

         return redirect('/dataarisan')->with('after_save', $after_save);

    }

    public function deletearisan($id){

      DB::delete('delete from data_arisan where id_data = ?',[$id]);
      $after_save = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Data Anggota ',
                'text-2' => 'Telah dihapus.'
            ];
      return redirect('/dataarisan')->with('after_save', $after_save);

    }

    public function bayararisan($id){

      $perubahan = 'Sudah Bayar';
      $data= Arisan::where('id_data', $id);
      $data->update([
        'status_bayar' => $perubahan
      ]);
      $after_save = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Salah satu anggota',
                'text-2' => 'Telah membayar.'
            ];
      return redirect('/dataarisan')->with('after_save', $after_save);

  }

  public function menangarisan(){
    $winner = DB::select("SELECT id_data FROM `data_arisan` WHERE status_menang='Belum Menang' ORDER BY RAND() LIMIT 1");
    foreach($winner as $data){
      $id = $data->id_data;
      $edit= Arisan::where('id_data', $id);
      $edit->update([
        'status_menang' => 'Sudah Menang'
      ]);

      $pemenang = \App\Arisan::where('id_data', $id)->first();
      return view("page.pemenangarisan",compact('pemenang'));
    }

  }

}
