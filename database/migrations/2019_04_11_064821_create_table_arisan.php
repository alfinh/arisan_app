<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArisan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_arisan', function (Blueprint $table) {
            $table->increments('id_data');
            $table->string('nm_anggota',255);
            $table->text('alamat');
            $table->text('keterangan');
            $table->enum('status_bayar',['Belum Bayar','Sudah Bayar']);
            $table->enum('status_menang',['Belum Menang','Sudah Menang']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_arisan');
    }
}
