<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    $now =Carbon::now();
    $kategori = new \App\Kategori;
    $kategori->kategori = 'Umum';
    $kategori->deskripsi = 'Semua bisa';
    $kategori->ket_waktu = $now;
    $kategori->id_akun = 1;
    $kategori->save();

    }
}
