<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class LoginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $now =Carbon::now();
      DB::table('user')->insert([
        ['nm_akun'=>'User', 'username'=>'user', 'password'=>bcrypt('user12345'), 'isadmin'=>0, 'alamat'=>'Jln.User Jauh', 'telepon'=>'0881738678451', 'negara'=>'Indonesia', 'created_at'=>$now],
        ['nm_akun'=>'Admin', 'username'=>'admin', 'password'=>bcrypt('admin12345'), 'isadmin'=>1, 'alamat'=>'NotForAdmin', 'telepon'=>'NotForAdmin', 'negara'=>'NotForAdmin', 'created_at'=>$now]
        ]);
    }
}
