# Aplikasi Manajemen Arisan
Aplikasi Ini merupakan aplikasi simple berbasis web untuk menampung alur konsep arisan dimulai dari penambahan data anggota, pembayaran, dan sampai pengocokan.
Aplikasi ini dibuat menggunakan framework laravel 5.7
### 3 Point Penting diaplikasi ini : 
1. Anggota
          
2. Pembayaran
          
3. Pengocokan Arisan

          
Untuk menggunakan aplikasi ini anda bisa mengclonenya.

# Panduan Menggunakan Aplikasi

## 1. Dasbor
Dasbor digunakan sebagai tampilan pertama yang menyajikan data statistik umum sebagai informasi.
![Beranda](ssaplikasi/dasbor.PNG)

## 2. Menampilkan Data
Menampilkan Data : Page ini berisikan semua data yang ditampilkan yang telah disusun sesuai yang dibutuhkan .
![adddata](ssaplikasi/readdata.PNG)

## 3. Tambah Data
Tambah Data : Page ini berisikan form  yang anda harus isi yang nantinya akan langsung tersimpan kedatabase .
![adddata](ssaplikasi/adddata.PNG)

## 4. Ubah data
Ubah Data : Page ini anda bisa mengubah data dan perubahanya akan langsung tersimpan kedatabase .
![editdata](ssaplikasi/editdata.PNG)

## 5. Hapus data
Hapus Data : Anda bisa menghapus data yang nantinya akan langsung muncul alert pemberitahuan. Dengan contoh saya menghapus Anggota arisan "Zeyn Nur Hakim" Anda bisa
mengamati difoto sebelumnya masih ada.
![hapusdata](ssaplikasi/hapusdata.PNG)

## 6. Fitur Bayar
Bayar : Anda bisa mengklik ini dan nantinya status pembayaran akan berubah sendirinya dengan contoh disini saya melakukan pembayaran atas nama "Alfi" dengan
otomatis button bayar langsung menghilang dengan sendirinya.
![bayardata](ssaplikasi/bayardata.PNG)

## 7. Fitur Pencarian
Fitur Pencarian : Anda bisa melakukan pencarian secara live dengan tidak meload page semuanya dikarenakan fitur ini menggunakan ajax.
![searchdata](ssaplikasi/searchdata.PNG)

## 7. Pengocokan Arisan
Pengocokan arisan : Fitur ini digunakan untuk mencari pemenang arisan dengan cara kamu harus mengklik button pengocokan (dibawah ini).
![btndata](ssaplikasi/btnkocok.PNG)
dan jika kamu sudah mengkliknya, Aplikasi ini akan mencari pemenangnya dengan cara dirandom number idnya oleh karena itu disini keburuntungan terpakai.
Disini anda harus menunggu 10 detik untuk anda tahu siapa pemenang arisannya.
![loaddata](ssaplikasi/loaddata.PNG)
Dan setelah itu akan muncul keterangan nama anggota dan data lainya.
![hasildata](ssaplikasi/hasildata.PNG)
dan yang terakhir, perubahan status menang itu akan langsung otomatis tersimpan kedatabase.
![lastdata](ssaplikasi/lastdata.PNG)


Dan ini sedikit penjelasan tentang aplikasi manajemen arisan ini semoga membantu dan berguna. 
Terima kasih :)


