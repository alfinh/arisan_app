<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
	  <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
	  <title>Manajemen Arisan</title>
	  <!-- plugins:css -->
    <link href="{{asset('assets')}}/fonts/font-awesome.css" rel="stylesheet" />
		<link href="{{asset('assets')}}/fonts/font-awesome.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="{{asset('assets')}}/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
	  <link rel="stylesheet" href="{{asset('assets')}}/vendors/css/vendor.bundle.base.css">
	  <link rel="stylesheet" href="{{asset('assets')}}/vendors/css/vendor.bundle.addons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{asset('assets')}}/css/pe-icon-7-stroke.css" rel="stylesheet" />
    @stack('css')
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link href="{{asset('assets')}}/css/jquery.dataTables.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('assets')}}/css/style.css">
  <!-- endinject -->

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
          <img src="{{asset('assets')}}/img/logotitile.png" style="width:75px; height:45px;" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
          <img src="{{asset('assets')}}/images/logo-mini.svg" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          <li class="nav-item active">
            <a href="#" class="nav-link">
              <i class="mdi mdi-elevation-rise"></i>Aplikasi Manajemen Arisan</a>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, Alfi Nur Hakim!</span>
              <img class="img-xs rounded-circle" src="{{asset('assets')}}/img/images.png" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item p-0">
                <div class="d-flex border-bottom">
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                    <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                  </div>
                </div>
              </a>
              <a class="dropdown-item mt-2">
                Manage Accounts
              </a>
              <a class="dropdown-item" data-toggle="modal" data-target="#ubahpassword">
                Change Password
              </a>

              <a class="dropdown-item">
                Check Inbox
              </a>
              <a class="dropdown-item">
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="{{asset('assets')}}/img/images.png" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Alfi Nur Hakim</p>
                  <div>
                    <small class="designation text-muted">Administrator</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
              <button class="btn btn-primary btn-block">New Fitur
                <i class="mdi mdi-plus"></i>
              </button>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{URL('/')}}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dasbor</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{  URL('/dataarisan')}}">
              <i class="menu-icon mdi mdi-chart-line"></i>
              <span class="menu-title">Data Arisan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="menu-icon mdi mdi-settings"></i>
              <span class="menu-title">Pengaturan</span>
            </a>
          </li>
        </ul>
      </nav>
			<div class="modal fade" tabindex="-1" id="ubahpassword" role="dialog" data-backdrop="false" style="background:#eee;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
			<!-- Modal Header -->
					<div class="modal-header">
						<h2>Ubah Password</h2>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"> &times;</span>
						</button>
								<!-- <h3 class="modal-title"><b>Ubah Password</b></h3> -->
					</div>

							<form action="{{ URL('/ubahpassword') }}" method="POST" autocomplete="off">
								 {{ csrf_field() }}
								 <div class="modal-body">

									 <div class="form-group" style="position: relative; top:10px;">
										 <label for="name">Password Baru</label>
										 <input type="password"  name="passwordbaru" id="myInput"  class="form-control" placeholder="Masukkan Password Baru" pattern=".{8,}"   required title="8 characters minimum" >
									 </div>

									 <div class="form-group" style="position: relative; top:10px;">
										 <label for="name">Confirm Password</label>
										 <input type="password"  name="passwordconfirm" id="myInput2" class="form-control" placeholder="Ketik ulang password baru" pattern=".{8,}"   required title="8 characters minimum" >
									 </div>
									 <label for="name"><input type="checkbox" onclick="myFunction()" style="margin-top:5px;"> Show Password</i></label>
								</div>

								<div class="modal-footer">
									<button type="reset" class="btn btn-raised btn-fill btn-warning ">Reset</button>
									<button type="submit" class="btn btn-primary btn-save btn-fill pull-right"> Submit</button>
								</div>
							</form>
						</div>
				 </div>
		</div>
      <!-- partial -->
       @yield('content')
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
              <a href="#" target="_blank">AMP</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Aplikasi Manajemen Pemasaran
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
	<script>
function myFunction() {
        var x = document.getElementById("myInput");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }

        var x = document.getElementById("myInput2");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
//</script>
  <script src="{{asset('assets')}}/js/jquery-1.10.2.js"></script>
  <script src="{{asset('assets')}}/js/jquery.3.2.1.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="{{asset('assets')}}/js/jquery.min.js"></script>
  <script type="text/javascript" src="{{asset('assets')}}/js/dataTables/jquery.dataTables.min.js"></script>
  <script src="{{asset('assets')}}/js/bootstrap.min.js"></script>
  <script src="{{asset('assets')}}/vendors/js/vendor.bundle.base.js"></script>
  <script src="{{asset('assets')}}/vendors/js/vendor.bundle.addons.js"></script>
  @stack('scripts')
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{asset('assets')}}/js/off-canvas.js"></script>
  <script src="{{asset('assets')}}/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('assets')}}/js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>
