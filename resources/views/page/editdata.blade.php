@extends('template.master')

@section('content')
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row purchace-popup">
      <div class="col-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <span style="font-size:17px;">Edit Data Anggota </span>
          </div><hr>
            <div class="col-sm-11" style="padding-left:30px;">
                 <form action="{{ URL('/updateanggota')}}" method="POST" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_data" value="{{$data->id_data}}">
                    <div class="modal-body">
                      <div class="form-group" style="position: relative; top:10px;">
                        <label for="name">Nama Anggota</label>
                        <input type="text"  name="nm_anggota" class="form-control" placeholder="Masukkan Nama" value="{{$data->nm_anggota}}" maxlength="200" required>
                      </div>

                      <div class="form-group"  >
                        <label for="name">Alamat</label>
                        <textarea  name="alamat" class="form-control" placeholder="Masukkan Alamat" maxlength="300"  required>{{$data->alamat}}</textarea>
                     </div>

                     <div class="form-group"  >
                       <label for="name">keterangan</label>
                       <textarea  name="keterangan" class="form-control" placeholder="Masukkan Keterangan" maxlength="300" required>{{$data->keterangan}}</textarea>
                    </div>

                   </div>

                   <div class="modal-footer">
                     <a href="{{url('/dataarisan')}}" class="btn btn-warning btn-fill pull-right">Kembali</a>
                     <button type="submit" class="btn btn-primary btn-save btn-fill pull-right"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                   </div>
                 </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
