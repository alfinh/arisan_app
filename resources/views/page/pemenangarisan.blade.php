<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
	  <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
	  <title>Manajemen Arisan</title>
	  <!-- plugins:css -->
    <link href="{{asset('assets')}}/fonts/font-awesome.css" rel="stylesheet" />
		<link href="{{asset('assets')}}/fonts/font-awesome.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="{{asset('assets')}}/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
	  <link rel="stylesheet" href="{{asset('assets')}}/vendors/css/vendor.bundle.base.css">
	  <link rel="stylesheet" href="{{asset('assets')}}/vendors/css/vendor.bundle.addons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{asset('assets')}}/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <style>
/*-----------
preloader
------------*/

#preloader {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 99999999999;
  background: white;
}
#loader {
	display: block;
	position: relative;
	left: 50%;
	top: 40%;
	width: 130px;
	height: 130px;
	margin: -75px 0 0 -75px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #384E6F;
	-webkit-animation: spin 2s linear infinite;
	animation: spin 2s linear infinite;
}
#loader:before {
	content: "";
	position: absolute;
	top: 5px;
	left: 5px;
	right: 5px;
	bottom: 5px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: orange;
	-webkit-animation: spin 3s linear infinite;
	animation: spin 3s linear infinite;
}
#loader:after {
	content: "";
	position: absolute;
	top: 15px;
	left: 15px;
	right: 15px;
	bottom: 15px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #6D9CE4;
	-webkit-animation: spin 1.5s linear infinite;
	animation: spin 1.5s linear infinite;
}
@-webkit-keyframes spin {
	0%   {
			-webkit-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			transform: rotate(0deg);
	}
	100% {
			-webkit-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			transform: rotate(360deg);
	}
}
@keyframes spin {
	0%   {
			-webkit-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			transform: rotate(0deg);
	}
	100% {
			-webkit-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			transform: rotate(360deg);
	}
}
</style>
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link href="{{asset('assets')}}/css/jquery.dataTables.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('assets')}}/css/style.css">
  <!-- endinject -->

</head>

<body>
  <div id="preloader">
    <div id="loader">
      <h2 style="color:skyblue; font-size:90px; padding-left:50px;">!</h2>
  </div>
  <h2 style="color:skyblue;	position: relative;
  	left: 50%;
    font-size:30px;
  	top: 52%;
  	width: 130px;
  	height: 130px;
  	margin: -75px 0 0 -75px;"><b>Finding Winner</b> </h2>
  </div>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
          <img src="{{asset('assets')}}/img/logotitile.png" style="width:75px; height:45px;" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
          <img src="{{asset('assets')}}/images/logo-mini.svg" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          <li class="nav-item active">
            <a href="#" class="nav-link">
              <i class="mdi mdi-elevation-rise"></i>Aplikasi Manajemen Arisan</a>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, Alfi Nur Hakim!</span>
              <img class="img-xs rounded-circle" src="{{asset('assets')}}/img/images.png" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item p-0">
                <div class="d-flex border-bottom">
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                    <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                  </div>
                </div>
              </a>
              <a class="dropdown-item mt-2">
                Manage Accounts
              </a>
              <a class="dropdown-item" data-toggle="modal" data-target="#ubahpassword">
                Change Password
              </a>

              <a class="dropdown-item">
                Check Inbox
              </a>
              <a class="dropdown-item">
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="{{asset('assets')}}/img/images.png" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Alfi Nur Hakim</p>
                  <div>
                    <small class="designation text-muted">Administrator</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
              <button class="btn btn-primary btn-block">New Fitur
                <i class="mdi mdi-plus"></i>
              </button>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{URL('/')}}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashbor</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{  URL('/dataarisan')}}">
              <i class="menu-icon mdi mdi-chart-line"></i>
              <span class="menu-title">Data Arisan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="menu-icon mdi mdi-settings"></i>
              <span class="menu-title">Pengaturan</span>
            </a>
          </li>
        </ul>
      </nav>
			<div class="modal fade" tabindex="-1" id="ubahpassword" role="dialog" data-backdrop="false" style="background:#eee;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
			<!-- Modal Header -->
					<div class="modal-header">
						<h2>Ubah Password</h2>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"> &times;</span>
						</button>
								<!-- <h3 class="modal-title"><b>Ubah Password</b></h3> -->
					</div>

							<form action="{{ URL('/ubahpassword') }}" method="POST" autocomplete="off">
								 {{ csrf_field() }}
								 <div class="modal-body">

									 <div class="form-group" style="position: relative; top:10px;">
										 <label for="name">Password Baru</label>
										 <input type="password"  name="passwordbaru" id="myInput"  class="form-control" placeholder="Masukkan Password Baru" pattern=".{8,}"   required title="8 characters minimum" >
									 </div>

									 <div class="form-group" style="position: relative; top:10px;">
										 <label for="name">Confirm Password</label>
										 <input type="password"  name="passwordconfirm" id="myInput2" class="form-control" placeholder="Ketik ulang password baru" pattern=".{8,}"   required title="8 characters minimum" >
									 </div>
									 <label for="name"><input type="checkbox" onclick="myFunction()" style="margin-top:5px;"> Show Password</i></label>
								</div>

								<div class="modal-footer">
									<button type="reset" class="btn btn-raised btn-fill btn-warning ">Reset</button>
									<button type="submit" class="btn btn-primary btn-save btn-fill pull-right"> Submit</button>
								</div>
							</form>
						</div>
				 </div>
		</div>
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row purchace-popup">
      <div class="col-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <span style="font-size:17px;">Data Arisan</span>
        </div><hr>
        <div class="row">
          <div class="col-sm-2 " style="margin-left:17px;">
            <a href="#" href="#" data-toggle="modal" data-target="#addForm"  class="btn btn-sm btn-primary"><i class="mdi mdi-plus"></i> Tambah</a>
          </div>
          <div class="col-sm-2" style="padding-bottom:10px;">
            <a href="#" class="btn btn-sm btn-primary pull-right"><i class="mdi mdi-briefcase"></i> Kocok Arisan</a>
          </div>
          <!-- <div class="col-sm-2">
            <p style="padding-top:10px;"><b><span style="font-size:15px; color:lightgreen;">Bayar(B)</span>,<span style="font-size:15px; color:orange;">Ubah(U)</span>,<span style="font-size:15px; color:crimson;">Hapus(H)</span> </b></p>
          </div> -->
        </div>
        <!-- Add Modal Add Anggota -->
      <div class="modal fade" tabindex="-1" id="addForm" role="dialog" data-backdrop="true">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
         <!-- Modal Header -->
           <div class="modal-header">
                 <h3 class="modal-title"><b>Tambah Anggota</b></h3>
           </div>

               <form action="{{ URL('/validasianggota')}}" method="POST" autocomplete="off">
                  {{ csrf_field() }}
                  <div class="modal-body">
                    <div class="form-group" style="position: relative; top:10px;">
                      <label for="name">Nama Anggota</label>
                      <input type="text"  name="nm_anggota" class="form-control" placeholder="Masukkan Nama" maxlength="200" required>
                    </div>

                    <div class="form-group"  >
                      <label for="name">Alamat</label>
                      <textarea  name="alamat" class="form-control" placeholder="Masukkan Alamat" maxlength="300" required></textarea>
                   </div>

                   <div class="form-group"  >
                     <label for="name">keterangan</label>
                     <textarea  name="keterangan" class="form-control" placeholder="Masukkan Keterangan" maxlength="300" required></textarea>
                  </div>

                 </div>

                 <div class="modal-footer">
                   <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-raised btn-fill btn-warning "><i class="glyphicon glyphicon-refresh"> </i> Kembali</button>
                   <button type="submit" class="btn btn-primary btn-save btn-fill pull-right"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                 </div>
               </form>
             </div>
          </div>
      </div>
        {{-- part alert --}}
               @if (Session::has('after_save'))
                       <div class="col-md-12" style="margin-top:20px;">
                           <div class="alert alert-xs alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                             <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong>{{ Session::get('after_save.title') }}</strong>
                             <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                           </div>
                       </div>
               @endif
         {{-- end part alert --}}
      <div class="panel-body">
        <div class="table-responsive">
        <table  class="table table-striped table-bordered table-primary table-md table-hover " id="tbl-arisan" >
        <thead>
            <tr>
              <td><b>No</b></td>
              <td><b>Nama Anggota</b></td>
              <td><b>Alamat</b></td>
              <td><b>Status Bayar</b></td>
              <td><b>Status menang</b></td>
              <td><b>Action</b></td>
              </tr>
          </thead>
            </table>
          </div>
        </div>
        </div>
        </div>
      </div>
    </div>

<center>
    <div class="modal fade" tabindex="-1" id="pemenang" role="dialog" data-backdrop="true">
     <div class="modal-dialog" role="document">
       <div class="modal-content" style="width:620px;">
       <!-- Modal Header -->
         <div class="modal-header">
               <center><h1 class="modal-title" style="color:skyblue; padding-left:40px;"><b>HASIL PENGOCOKAN ARISAN</b></h1></center>
         </div>
                <div class="modal-body">
                  <div class="col-sm-10 col-sm-1">
                    <img src="{{asset('assets')}}/img/pemenang.png" width="700" style="padding-right:230px;"></i>
                    <span style="font-size:40px; color:skyblue;"><b>SELAMAT UNTUK ANDA</b></span>
                    <span style="font-size:40px; color:skyblue;"><b>{{$pemenang->nm_anggota}}</b></span><br>
                    <span style="font-size:40px; color:skyblue;"><b>{{$pemenang->alamat}}</b></span>
               </div>
           </div>
           <div class="modal-footer">
            <center> <a href="{{url('/dataarisan')}}" class="btn btn-md btn-primary">Kembali</a> </center>
        </div>
    </div>
  </div>
</center>
<!-- partial:partials/_footer.html -->
<footer class="footer">
  <div class="container-fluid clearfix">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
      <a href="#" target="_blank">AMP</a>. All rights reserved.</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Aplikasi Manajemen Pemasaran
      <i class="mdi mdi-heart text-danger"></i>
    </span>
  </div>
</footer>
    <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- plugins:js -->
    <script>
    function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }

    var x = document.getElementById("myInput2");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
    }
    //</script>
    <script src="{{asset('assets')}}/js/jquery-1.10.2.js"></script>
    <script src="{{asset('assets')}}/js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets')}}/js/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset('assets')}}/js/dataTables/jquery.dataTables.min.js"></script>
    <script src="{{asset('assets')}}/js/bootstrap.min.js"></script>
    <script src="{{asset('assets')}}/vendors/js/vendor.bundle.base.js"></script>
    <script src="{{asset('assets')}}/vendors/js/vendor.bundle.addons.js"></script>

    <script type="text/javascript">
    $('#pemenang').modal({backdrop: 'static', keyboard: false});
    $(window).on("load", function () {
      $('#preloader').fadeOut(10000);
    });
    var t_outcome;

    $(function() {
        t_outcome = $('#tbl-arisan').DataTable({
            pagingType:"full_numbers",
            processing: true,
              "language": {
            processing: "<img src='{{asset('assets/img/2 (2).gif')}}'> "},
            serverSide: true,
            ajax:{
              url:'{{ route("dataarisan.json") }}'
          },
            columns: [
                { data: null, orderable: false},
                { data: 'nm_anggota', name: 'nm_anggota' },
                { data: 'alamat', name: 'alamat' },
                { data: 'status_bayar', name: 'status_bayar' },
                { data: 'status_menang', name: 'status_menang' },
                { data: 'action', orderable:false, searchable:false }
              ],
              dom: 'B<"toolbar">ifrtlp',
              "rowCallback": function (nRow, aData, iDisplayIndex) {
               var oSettings = this.fnSettings ();
               $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
               return nRow;
             }
     } );


     $(document).on('click', '.delete', function(){
           if(confirm("Are you sure you want to Delete this data?"))
           {
            alert('Record deleted successfully.'); window.location.href='/outcome';
           }
           else
           {
               return false;
           }
       });


     $('#tbl-user tbody').on( 'click','.lock', function () {
       alert("Maaf, bagian Ini belum berfungsi ");
     });
    });

    </script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{asset('assets')}}/js/off-canvas.js"></script>
    <script src="{{asset('assets')}}/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{asset('assets')}}/js/dashboard.js"></script>
    <!-- End custom js for this page-->
    </body>

    </html>
