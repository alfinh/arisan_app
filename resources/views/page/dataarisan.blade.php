@extends('template.master')

@section('content')
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row purchace-popup">
      <div class="col-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <span style="font-size:17px;">Data Arisan</span>
        </div><hr>
        <div class="row">
          <div class="col-sm-2 " style="margin-left:17px;">
            <a href="#" href="#" data-toggle="modal" data-target="#addForm"  class="btn btn-sm btn-primary"><i class="mdi mdi-plus"></i> Tambah</a>
          </div>
          <div class="col-sm-2" style="padding-bottom:10px;">
            <a href="{{url('/menangarisan')}}" class="btn btn-sm btn-primary pull-right"><i class="mdi mdi-briefcase"></i> Kocok Arisan</a>
          </div>
          <!-- <div class="col-sm-2">
            <p style="padding-top:10px;"><b><span style="font-size:15px; color:lightgreen;">Bayar(B)</span>,<span style="font-size:15px; color:orange;">Ubah(U)</span>,<span style="font-size:15px; color:crimson;">Hapus(H)</span> </b></p>
          </div> -->
        </div>
        <!-- Add Modal Add Anggota -->
      <div class="modal fade" tabindex="-1" id="addForm" role="dialog" data-backdrop="true">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
         <!-- Modal Header -->
           <div class="modal-header">
                 <h3 class="modal-title"><b>Tambah Anggota</b></h3>
           </div>

               <form action="{{ URL('/validasianggota')}}" method="POST" autocomplete="off">
                  {{ csrf_field() }}
                  <div class="modal-body">
                    <div class="form-group" style="position: relative; top:10px;">
                      <label for="name">Nama Anggota</label>
                      <input type="text"  name="nm_anggota" class="form-control" placeholder="Masukkan Nama" maxlength="200" required>
                    </div>

                    <div class="form-group"  >
                      <label for="name">Alamat</label>
                      <textarea  name="alamat" class="form-control" placeholder="Masukkan Alamat" maxlength="300" required></textarea>
                   </div>

                   <div class="form-group"  >
                     <label for="name">keterangan</label>
                     <textarea  name="keterangan" class="form-control" placeholder="Masukkan Keterangan" maxlength="300" required></textarea>
                  </div>

                 </div>

                 <div class="modal-footer">
                   <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-raised btn-fill btn-warning "><i class="glyphicon glyphicon-refresh"> </i> Kembali</button>
                   <button type="submit" class="btn btn-primary btn-save btn-fill pull-right"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                 </div>
               </form>
             </div>
          </div>
      </div>
        {{-- part alert --}}
               @if (Session::has('after_save'))
                       <div class="col-md-12" style="margin-top:20px;">
                           <div class="alert alert-xs alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                             <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong>{{ Session::get('after_save.title') }}</strong>
                             <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                           </div>
                       </div>
               @endif
         {{-- end part alert --}}
      <div class="panel-body">
        <div class="table-responsive">
        <table  class="table table-striped table-bordered table-primary table-md table-hover " id="tbl-arisan" >
        <thead>
            <tr>
              <td><b>No</b></td>
              <td><b>Nama Anggota</b></td>
              <td><b>Alamat</b></td>
              <td><b>Status Bayar</b></td>
              <td><b>Status menang</b></td>
              <td><b>Action</b></td>
              </tr>
          </thead>
            </table>
          </div>
        </div>
        </div>
        </div>
      </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript">
var t_outcome;

$(function() {
    t_outcome = $('#tbl-arisan').DataTable({
        pagingType:"full_numbers",
        processing: true,
          "language": {
        processing: "<img src='{{asset('assets/img/2 (2).gif')}}'> "},
        serverSide: true,
        ajax:{
          url:'{{ route("dataarisan.json") }}'
      },
        columns: [
            { data: null, orderable: false},
            { data: 'nm_anggota', name: 'nm_anggota' },
            { data: 'alamat', name: 'alamat' },
            { data: 'status_bayar', name: 'status_bayar' },
            { data: 'status_menang', name: 'status_menang' },
            { data: 'action', orderable:false, searchable:false }
          ],
          dom: 'B<"toolbar">ifrtlp',
          "rowCallback": function (nRow, aData, iDisplayIndex) {
           var oSettings = this.fnSettings ();
           $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
           return nRow;
         }
 } );


 $(document).on('click', '.delete', function(){
       if(confirm("Are you sure you want to Delete this data?"))
       {
        alert('Record deleted successfully.'); window.location.href='/outcome';
       }
       else
       {
           return false;
       }
   });


 $('#tbl-user tbody').on( 'click','.lock', function () {
   alert("Maaf, bagian Ini belum berfungsi ");
 });
});

</script>
@endpush
