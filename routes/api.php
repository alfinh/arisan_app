<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){

//profile
Route::get('details', 'API\UserController@details');
Route::post('logout','API\UserController@logoutApi');
Route::put('updateprofile','API\UserController@update');
Route::delete('deleteakun','API\UserController@delete');


//Income
Route::get('income','API\IncomeController@index');
Route::get('showincome/{id}', 'API\IncomeController@show');
Route::post('addincome', 'API\IncomeController@store');
Route::put('updateincome/{id}', 'API\IncomeController@update');
Route::delete('deleteincome/{id}', 'API\IncomeController@delete');

//Outcome
Route::get('outcome','API\OutcomeController@index');
Route::get('showoutcome/{id}', 'API\OutcomeController@show');
Route::post('addoutcome', 'API\OutcomeController@store');
Route::put('updateoutcome/{id}', 'API\OutcomeController@update');
Route::delete('deleteoutcome/{id}', 'API\OutcomeController@delete');

//kategori
Route::get('kategori','API\KategoriController@index');
Route::get('showkategori/{id}', 'API\KategoriController@show');
Route::post('addkategori', 'API\KategoriController@store');
Route::put('updatekategori/{id}', 'API\KategoriController@update');
Route::delete('deletekategori/{id}', 'API\KategoriController@delete');

});
