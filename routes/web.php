<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//allpage
Route::get('/','PageController@index');
Route::get('/dataarisan','PageController@getarisan');
Route::any('/dataarisan/json','PageController@json')->name('dataarisan.json');
Route::post('/validasianggota','Pagecontroller@addanggota');
Route::get('edit_arisan/{id}','PageController@showarisan');
Route::post('/updateanggota','PageController@updatearisan');
Route::get('delete_arisan/{id}','PageController@deletearisan');
Route::get('bayar_arisan/{id}','PageController@bayararisan');
Route::get('/menangarisan','PageController@menangarisan');
